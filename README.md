# SorobanReleaseRadar

## Overview
SorobanReleaseRadar is a backend service designed to monitor new version changes for Rust SDK and Soroban SDK. 
It also facilitates the initiation of the build process to create a new runtime image upon request from the admin panel. 

## Table of Contents
1. [Installation](#installation)
2. [Configuration](#configuration)
3. [Development](#development)
4. [Deployment](#deployment)
5. [License](#license)

## Installation
1. Clone the repository:
   ```bash
   git clone https://gitlab.com/soroban-explorer/soroban-release-radar.git
   cd SorobanReleaseRadar
   mvn clean install
   ```

## Configuration

Update the necessary configurations in the src/main/resources/application.yml file.
Configure the admin panel interface for initiating build processes and starting application services.


## Development

Development Workflow:

Develop backend services for monitoring new version changes for Rust SDK and Soroban SDK.
Implement functionality to initiate the build process for creating a new runtime image.

## Deployment

Deploy the built JAR file to your desired environment.
Ensure Docker images are built and tagged correctly for runtime image creation.

## License
This project is licensed under the GNU GENERAL PUBLIC LICENSE.