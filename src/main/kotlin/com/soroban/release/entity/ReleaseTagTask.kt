package com.soroban.release.entity

import jakarta.persistence.*
import lombok.Getter
import lombok.Setter
import java.util.*

@Getter
@Setter
@Entity
@Table(name = "release_tag_tasks")
class ReleaseTagTask {
    public constructor() {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null

    var tag: String? = "";

    @Enumerated(EnumType.STRING)
    @Column(name = "tag_type")
    var tagType: TagType = TagType.NONE

    @Enumerated(EnumType.STRING)
    var status: TaskStatus = TaskStatus.NEW

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    var createdDate: Date? = null

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_update_date")
    var lastUpdateDate: Date? = null

}

enum class TagType {
    RUST,
    SOROBAN,
    OTHER,
    NONE
}

enum class TaskStatus {
    NEW,
    COMPLETE
}