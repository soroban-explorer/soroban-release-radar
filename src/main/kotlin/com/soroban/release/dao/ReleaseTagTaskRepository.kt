package com.soroban.release.dao

import com.soroban.release.entity.ReleaseTagTask
import com.soroban.release.entity.TagType
import com.soroban.release.entity.TaskStatus
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ReleaseTagTaskRepository : JpaRepository<ReleaseTagTask, Long> {
    @Query(value = "SELECT * FROM release_tag_tasks WHERE tag_type = 'RUST' AND tag IN (:tags)", nativeQuery = true)
    fun findExistingRustTags(tags: List<String>): List<ReleaseTagTask>

    @Query(value = "SELECT MAX(tag) FROM release_tag_tasks WHERE tag_type = 'RUST'", nativeQuery = true)
    fun findMostRecentRustTag(): String?

    @Query(value = "SELECT * FROM release_tag_tasks WHERE tag_type = 'SOROBAN' AND tag IN (:tags)", nativeQuery = true)
    fun findExistingSorobanTags(tags: List<String>): List<ReleaseTagTask>

    @Query(value = "SELECT MAX(tag) FROM release_tag_tasks WHERE tag_type = 'SOROBAN'", nativeQuery = true)
    fun findMostRecentSorobanTag(): String?
    // Define a custom query method to find a ReleaseTagTask by tag
    fun findByTag(tag: String): ReleaseTagTask?

    /**
     * Finds tasks with status "NEW" based on the specified tag type.
     *
     * @param tagType The tag type to filter tasks.
     * @return A list of ReleaseTagTask entities with status "NEW" and the specified tag type.
     */
    fun findByTagTypeAndStatus(tagType: TagType, status: TaskStatus): List<ReleaseTagTask>
}