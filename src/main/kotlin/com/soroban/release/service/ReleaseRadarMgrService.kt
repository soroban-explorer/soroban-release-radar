package com.soroban.release.service

import com.soroban.release.dao.ReleaseTagTaskRepository
import com.soroban.release.entity.ReleaseTagTask
import com.soroban.release.entity.TagType
import com.soroban.release.entity.TaskStatus
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class ReleaseRadarMgrService(private val releaseTagTaskRepo: ReleaseTagTaskRepository) {
    /**
     * Loads new release Rust release versions from the database.
     *
     * @return A Mono emitting a list of ReleaseTagTask entities with status "NEW" and tag type "RUST".
     */
    fun loadNewReleaseRustReleaseVersions() : Mono<List<ReleaseTagTask>> {
        return Mono.just(releaseTagTaskRepo.findByTagTypeAndStatus(TagType.RUST, TaskStatus.NEW))
    }
    /**
     * Loads new release Soroban release versions from the database.
     *
     * @return A Mono emitting a list of ReleaseTagTask entities with status "NEW" and tag type "SOROBAN".
     */
    fun loadNewReleaseSorobanReleaseVersions() : Mono<List<ReleaseTagTask>> {
        return Mono.just(releaseTagTaskRepo.findByTagTypeAndStatus(TagType.SOROBAN, TaskStatus.NEW))
    }
}