package com.soroban.release.service.common

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder
import com.amazonaws.services.simpleemail.model.*
import com.soroban.release.model.Mail
import com.soroban.release.util.Utils
import jakarta.annotation.PostConstruct
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.env.Environment
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.thymeleaf.context.Context
import org.thymeleaf.spring6.SpringTemplateEngine

/**
 * Service class responsible for sending notifications, such as emails.
 * @author Brian
 */
@Service
class NotificationService {
    private val logger = LoggerFactory.getLogger(NotificationService::class.java)

    @Autowired
    private var templateEngine: SpringTemplateEngine? = null

    @Autowired
    private var env: Environment? = null
    @Value("\${app.email.digest}")
    private val CONFIGSET: String? = null
    @Value("\${app.cloud.region}")
    private val REGION: String? = null

    /**
     * Asynchronously sends an email using the provided Mail object.
     *
     * This method is marked as asynchronous, meaning it will be executed in a separate thread.
     * The actual sending of the email is performed in the background, allowing the caller to
     * continue executing other tasks without waiting for the email to be sent.
     *
     * @param mail The Mail object containing the email details such as sender, recipient, subject, etc.
     */
    @Async
    fun sendMessage(mail: Mail) {
        try {
            // Fetch access key and secret key from environment variables or system properties
            val accessKey = Utils.getConfigVal("app.cloud.accesskey", env!!)
            val secretKey = Utils.getConfigVal("app.cloud.secretkey", env!!)

            // Use DefaultAWSCredentialsProviderChain to load credentials securely from IAM roles
            val credentials = if (accessKey.isNullOrEmpty() || secretKey.isNullOrEmpty()) {
                // If access key or secret key is not provided, use the default credential provider chain
                // This will automatically fetch credentials securely from IAM roles in a production environment
                DefaultAWSCredentialsProviderChain().credentials
            } else {
                // If access key and secret key are provided, create BasicAWSCredentials, local or dev environment
                BasicAWSCredentials(accessKey, secretKey)
            }
            val client = AmazonSimpleEmailServiceClientBuilder.standard()
                .withRegion(REGION)
                .withCredentials(AWSStaticCredentialsProvider(credentials))
                .build()

            val context = Context()
            context.setVariables(mail.model)

            val subject = mail.subject
            val htmlBody = templateEngine!!.process(mail.template, context)
            val to = mail.to
            val from = mail.from

            val request = SendEmailRequest()
                .withDestination(Destination().withToAddresses(to))
                .withMessage(
                    Message()
                        .withBody(Body().withHtml(Content().withCharset("UTF-8").withData(htmlBody)))
                        .withSubject(Content().withCharset("UTF-8").withData(subject))
                )
                .withSource(from)
                .withConfigurationSetName(CONFIGSET)

            client.sendEmail(request)
            logger.info("Notification sent!")
        } catch (ex: Exception) {
            logger.error("The email was not sent. Error message: ${ex.message}")
        }
    }
}