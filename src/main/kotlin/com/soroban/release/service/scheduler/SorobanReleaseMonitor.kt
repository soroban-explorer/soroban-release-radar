package com.soroban.release.service.scheduler

import com.soroban.release.service.release.SorobanReleaseRadarService
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

/**
 * Monitors Soroban releases and triggers appropriate actions.
 */
@Component
@EnableScheduling
@ConditionalOnExpression("\${app.schedule.soroban.enable:true}")
class SorobanReleaseMonitor (val sorobanReleaseRadarService: SorobanReleaseRadarService) {
    /**
     * Checks for new Soroban releases based on the configured schedule.
     */
    @Scheduled(cron = "\${app.schedule.soroban.cron}")
    fun checkSorobanRelease() {
        sorobanReleaseRadarService.findNewReleaseVersions()
    }
}