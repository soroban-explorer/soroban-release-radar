package com.soroban.release.service.scheduler

import com.soroban.release.service.release.RustReleaseRadarService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
/**
 * Monitors Rust releases and triggers appropriate actions.
 */
@Component
@EnableScheduling
@ConditionalOnExpression("\${app.schedule.rust.enable:true}")
class RustReleaseMonitor(val rustReleaseRadarService: RustReleaseRadarService) {
    /**
     * Checks for new Rust releases based on the configured schedule.
     */
    @Scheduled(cron = "\${app.schedule.rust.cron}")
    fun checkRustRelease() {
        rustReleaseRadarService.findNewReleaseVersions()
    }
}