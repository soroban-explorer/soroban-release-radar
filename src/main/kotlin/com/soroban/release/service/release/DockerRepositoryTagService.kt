package com.soroban.release.service.release

import com.google.gson.Gson
import com.soroban.release.model.Release
import com.soroban.release.model.TagsResponse
import org.springframework.stereotype.Service
import java.net.URL

/**
 * Implementation of [RepositoryTagService] that retrieves tags from a Docker hub images.
 * @author Brian
 */
@Service("dockerRepositoryTagService")
class DockerRepositoryTagService: RepositoryTagService {
    /**
     * Retrieves a list of tags for the specified Docker repository, filtered by the provided criteria.
     *
     * @param repo The name of the Docker repository.
     * @param pageSize The number of tags to fetch per page (default is 100).
     * @param filter A predicate function to filter the tags. Only tags for which this function returns true will be included.
     * @return A list of [Release] objects representing the tags that meet the filter criteria, sorted by tag name.
     */
    override fun listAllTags(repo: String, pageSize: Int, filter: (String) -> Boolean): List<String> {
        require(repo.isNotBlank()) { "Usage: listTags <repoName> [page_size]" }
        val baseUrl = "https://registry.hub.docker.com/api/content/v1/repositories/public/library/$repo/tags"

        val gson = Gson()
        val firstPageUrl = "$baseUrl?page_size=$pageSize&page=1"
        val firstPageJson = URL(firstPageUrl).readText()
        val firstPageResponse = gson.fromJson(firstPageJson, TagsResponse::class.java)

        val allTags = firstPageResponse.results.map { it.name }.toMutableList()
        val pageCount = (firstPageResponse.count + pageSize - 1) / pageSize

        for (page in 2..pageCount) {
            val pageUrl = "$baseUrl?page_size=$pageSize&page=$page"
            val pageJson = URL(pageUrl).readText()
            val pageResponse = gson.fromJson(pageJson, TagsResponse::class.java)
            allTags.addAll(pageResponse.results.map { it.name })
        }

        return allTags.filter { filter(it) }.sortedBy { it }
    }
}