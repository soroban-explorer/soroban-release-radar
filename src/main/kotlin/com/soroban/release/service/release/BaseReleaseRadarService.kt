package com.soroban.release.service.release


import com.soroban.release.conf.AppConfig
import com.soroban.release.entity.ReleaseTagTask
import com.soroban.release.entity.TagType
import com.soroban.release.model.Mail
import com.soroban.release.model.Release
import com.soroban.release.model.ReleaseRes
import com.soroban.release.service.common.NotificationService
import org.slf4j.Logger
import reactor.core.publisher.Mono

open abstract class BaseReleaseRadarService {
    abstract fun getReleaseVersions(minVersion: String?): List<String>

    abstract fun getRepositoryDescription(): String

    abstract fun saveNewReleaseTag(tag: String, tagType: TagType): ReleaseTagTask

    fun findNewReleaseVersions(): Mono<ReleaseRes> {
        val releaseRes = ReleaseRes()
        releaseRes.description = getRepositoryDescription()
        val mostRecentTag = findMostRecentTag()
        val newTags = getReleaseVersions(mostRecentTag)
        newTags.forEach { tag ->
            getLog().info("Processing {} - new tag: {}", getTagType(), tag)
            releaseRes.releases.add(Release(tag))
            val releaseTagTask = saveNewReleaseTag(tag, getTagType())
            getLog().info("New {} tag saved: {}", getTagType(), tag)
            sendNewReleaseMessage(releaseTagTask)
            getLog().info("Notification sent for {} tag: {}", getTagType(), tag)
        }
        if(newTags.isNotEmpty()) {
            getLog().info("New {} release versions processed.", getTagType())
        }

        return Mono.just(releaseRes)
    }
    protected abstract fun findMostRecentTag(): String?

    protected abstract fun getTagType(): TagType

    protected abstract fun getAppConfig(): AppConfig

    protected abstract fun getNotificationService(): NotificationService
    protected abstract fun getLog(): Logger
    // Method to send a message asynchronously
    private fun sendMessage(mail: Mail) {
        // Call the sendMessage method of the MailSender bean
        // Since sendMessage is marked as @Async, it will execute asynchronously
        getNotificationService().sendMessage(mail)
    }
    /**
     * Sends a notification email for a new release.
     *
     * This method checks if email notifications are enabled in the application configuration.
     * If enabled, it constructs an email message using the configured email properties and sends it.
     *
     * @param releaseTagTask The release tag task for which the notification is sent.
     */
    protected fun sendNewReleaseMessage(releaseTagTask: ReleaseTagTask) {
        if(getAppConfig().email.enable) {
            val emailProp = getAppConfig().email
            val subject = "New ${releaseTagTask.tagType} Release Version - ${releaseTagTask.tag}"
            val mail = Mail(emailProp.from, emailProp.to, subject)
            mail.template = emailProp.template.release
            mail.addModel("username", "Operator");
            mail.addModel("tag", releaseTagTask.tag!!)
            mail.addModel("tagType", releaseTagTask.tagType!!)
            sendMessage(mail)
        }
    }

}