package com.soroban.release.service.release

import com.soroban.release.conf.AppConfig
import com.soroban.release.dao.ReleaseTagTaskRepository
import com.soroban.release.entity.ReleaseTagTask
import com.soroban.release.entity.TagType
import com.soroban.release.entity.TaskStatus
import com.soroban.release.service.common.NotificationService
import com.soroban.release.util.Constants
import com.soroban.release.util.Utils
import jakarta.transaction.Transactional
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service

/**
 * Service class responsible for managing rust docker release radar functionality.
 *  @author Brian
 */
@Service
@Transactional
class RustReleaseRadarService(
    @Qualifier("dockerRepositoryTagService") private val dockerTagService: RepositoryTagService,
    private val appConfig: AppConfig,
    private var notificationService: NotificationService,
    private val releaseTagTaskRepo: ReleaseTagTaskRepository
) : BaseReleaseRadarService() {

    private val logger: Logger = LoggerFactory.getLogger(RustReleaseRadarService::class.java)
    override fun getReleaseVersions(minVersion: String?): List<String> {
        logger.info("Searching for ${getRepositoryDescription()} releases...")
        val repo = Constants.REPO_RUST
        val filter: (String) -> Boolean = {
            if (minVersion.isNullOrEmpty()) {
                true
            } else {
                it > minVersion && it.matches("""^1\.\d+\.\d+$""".toRegex())
            }
        }
        return dockerTagService.listAllTags(repo, 100, filter)
    }

    override fun getRepositoryDescription(): String {
        return Constants.REPO_RUST
    }

    override fun saveNewReleaseTag(tag: String, tagType: TagType): ReleaseTagTask {
        logger.info("Saving new ${getRepositoryDescription()} release tag: $tag...")
        val now = Utils.getCurrent()
        val releaseTagTask = ReleaseTagTask()
        releaseTagTask.tag = tag
        releaseTagTask.tagType = tagType
        releaseTagTask.status = TaskStatus.NEW
        releaseTagTask.createdDate = now
        releaseTagTask.lastUpdateDate = now
        releaseTagTaskRepo.save(releaseTagTask)
        return releaseTagTask
    }

    override fun findMostRecentTag(): String? {
        return releaseTagTaskRepo.findMostRecentRustTag()
    }

    override fun getTagType(): TagType {
        return TagType.RUST
    }

    override fun getAppConfig(): AppConfig {
        return this.appConfig
    }

    override fun getNotificationService(): NotificationService {
        return this.notificationService
    }

    override fun getLog(): Logger {
        return this.logger
    }
}