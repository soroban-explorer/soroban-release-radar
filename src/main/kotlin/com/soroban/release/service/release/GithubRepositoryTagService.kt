package com.soroban.release.service.release
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.soroban.release.model.Tag
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
/**
 * Implementation of [RepositoryTagService] that retrieves tags from a GitHub repository.
 *  @author Brian
 */
@Service("githubRepositoryTagService")
class GithubRepositoryTagService: RepositoryTagService {

    /**
     * Retrieves all tags for the specified GitHub repository.
     *
     * @param repo The name of the GitHub repository.
     * @param pageSize The maximum number of tags to retrieve per page.
     * @param filter A filter function to apply on the retrieved tags. Only tags that satisfy this filter will be included.
     * @return A list of tag names that match the filter criteria.
     * @throws RuntimeException if failed to fetch tags from GitHub.
     */
    override fun listAllTags(repo: String, pageSize: Int, filter: (String) -> Boolean): List<String> {
        val webClient = WebClient.create("https://api.github.com")
        val url = "/repos/$repo/tags?per_page=$pageSize"

        val response = webClient.get()
            .uri(url)
            .retrieve()
            .bodyToMono<String>()
            .block() ?: throw RuntimeException("Failed to fetch tags from GitHub")

        val objectMapper = jacksonObjectMapper()
        val tagsResponse = objectMapper.readValue(response, Array<Tag>::class.java)
        val tags = tagsResponse.map { it.name }.filter(filter)
        return tags
    }
}