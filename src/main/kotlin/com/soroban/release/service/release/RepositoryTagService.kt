package com.soroban.release.service.release

interface RepositoryTagService {
    fun listAllTags(repo: String, pageSize: Int = 100, filter: (String) -> Boolean): List<String>
}