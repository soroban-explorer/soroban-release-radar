package com.soroban.release.service.release

import com.soroban.release.conf.AppConfig
import com.soroban.release.dao.ReleaseTagTaskRepository
import com.soroban.release.entity.ReleaseTagTask
import com.soroban.release.entity.TagType
import com.soroban.release.entity.TaskStatus
import com.soroban.release.service.common.NotificationService
import com.soroban.release.util.Constants
import com.soroban.release.util.Utils
import jakarta.transaction.Transactional
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service

/**
 * Service class responsible for managing soroban release radar functionality.
 * @author Brian
 */
@Service
@Transactional
class SorobanReleaseRadarService(
    @Qualifier("githubRepositoryTagService") private val githubTagService: RepositoryTagService,
    private val appConfig: AppConfig,
    private var notificationService: NotificationService,
    private val releaseTagTaskRepo: ReleaseTagTaskRepository
) : BaseReleaseRadarService() {
    private val logger: Logger = LoggerFactory.getLogger(SorobanReleaseRadarService::class.java)
    override fun getReleaseVersions(minVersion: String?): List<String> {
        logger.info("Searching for ${getRepositoryDescription()} releases...")
        val repo = Constants.REPO_SOROBAN
        val filter: (String) -> Boolean = {
            if (minVersion.isNullOrEmpty()) {
                true
            } else {
                val versionRegex = """^v?(\d+\.\d+\.\d+)$""".toRegex()
                val minVersionMatch = versionRegex.find(minVersion)
                if (minVersionMatch != null) {
                    val minVersionNumber = minVersionMatch.groupValues[1]
                    val tagVersionMatch = versionRegex.find(it)
                    val tagVersionNumber = tagVersionMatch?.groupValues?.get(1)
                    tagVersionNumber != null && tagVersionNumber > minVersionNumber
                } else {
                    false
                }
            }
        }
        return githubTagService.listAllTags(repo, 50, filter)
    }

    override fun getRepositoryDescription(): String {
        return Constants.REPO_SOROBAN
    }

    override fun saveNewReleaseTag(tag: String, tagType: TagType): ReleaseTagTask {
        logger.info("Saving new ${getRepositoryDescription()} release tag: $tag...")
        val now = Utils.getCurrent()
        val releaseTagTask = ReleaseTagTask()
        releaseTagTask.tag = tag
        releaseTagTask.tagType = tagType
        releaseTagTask.status = TaskStatus.NEW
        releaseTagTask.createdDate = now
        releaseTagTask.lastUpdateDate = now
        releaseTagTaskRepo.save(releaseTagTask)
        return releaseTagTask
    }

    override fun findMostRecentTag(): String? {
        return releaseTagTaskRepo.findMostRecentSorobanTag()
    }

    override fun getTagType(): TagType {
        return TagType.SOROBAN
    }
    override fun getAppConfig(): AppConfig {
        return this.appConfig
    }
    override fun getNotificationService(): NotificationService {
        return this.notificationService
    }
    override fun getLog(): Logger {
        return this.logger
    }
}