package com.soroban.release.controller

import com.soroban.release.entity.ReleaseTagTask
import com.soroban.release.model.ReleaseRes
import com.soroban.release.service.ReleaseRadarMgrService
import com.soroban.release.service.release.RustReleaseRadarService
import com.soroban.release.service.release.SorobanReleaseRadarService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class ReleaseRadarController(
    val sorobanReleaseRadarService: SorobanReleaseRadarService,
    val rustReleaseRadarService: RustReleaseRadarService,
    val releaseRadarMgrService: ReleaseRadarMgrService
) {
    private val logger: Logger = LoggerFactory.getLogger(ReleaseRadarController::class.java)
    @RequestMapping(value = ["/rustVersion"], method = arrayOf(RequestMethod.GET))
    fun findNewReleaseRustReleaseVersions(): Mono<ReleaseRes> {
        return rustReleaseRadarService.findNewReleaseVersions()
    }
    @RequestMapping(value = ["/sorobanVersion"], method = arrayOf(RequestMethod.GET))
    fun findNewReleaseSorobanReleaseVersions(): Mono<ReleaseRes> {
        return sorobanReleaseRadarService.findNewReleaseVersions()
    }

    /**
     * Endpoint to load new Soroban release versions.
     *
     * @return A Mono emitting a list of ReleaseTagTask entities with status "NEW" and tag type "SOROBAN".
     */
    @GetMapping("/soroban/new")
    fun loadNewReleaseSorobanReleaseVersions(): Mono<List<ReleaseTagTask>> {
        return releaseRadarMgrService.loadNewReleaseSorobanReleaseVersions()
    }

    /**
     * Endpoint to load new Soroban release versions.
     *
     * @return A Mono emitting a list of ReleaseTagTask entities with status "NEW" and tag type "SOROBAN".
     */
    @GetMapping("/rust/new")
    fun loadNewReleaseRustReleaseVersions(): Mono<List<ReleaseTagTask>> {
        return releaseRadarMgrService.loadNewReleaseRustReleaseVersions()
    }

}