package com.soroban.release.conf

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "app")
class AppConfig {
    lateinit var email: EmailProperties
    lateinit var cloud: CloudProperties

    class EmailProperties {
        var enable: Boolean = false
        lateinit var digest: String
        lateinit var from: String
        lateinit var to: String
        lateinit var subject: String
        lateinit var template: TemplateProperties

        class TemplateProperties {
            lateinit var release: String
        }
    }

    class CloudProperties {
        lateinit var region: String
        lateinit var accesskey: String
        lateinit var secretkey: String
    }
}