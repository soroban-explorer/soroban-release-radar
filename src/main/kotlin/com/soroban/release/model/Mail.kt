package com.soroban.release.model

class Mail {
    var from: String? = null
    var to: String? = null
    var cc: String? = null
    var bcc: String? = null
    var subject: String? = null
    var template: String? = null
    val model: MutableMap<String, Any> = HashMap()
    val attachments: MutableMap<String, String> = HashMap()

    constructor()

    constructor(from: String?, to: String?, subject: String?) {
        this.from = from
        this.to = to
        this.subject = subject
    }

    fun addModel(key: String, value: Any) {
        model[key] = value
    }

    fun addAllModel(model: Map<String, String>) {
        this.model.putAll(model)
    }

    fun addAttachment(key: String, value: String) {
        attachments[key] = value
    }

    override fun toString(): String {
        return "Mail(from=$from, to=$to, subject=$subject)"
    }
}