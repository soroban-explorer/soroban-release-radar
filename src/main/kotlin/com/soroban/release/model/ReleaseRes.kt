package com.soroban.release.model

import lombok.Getter
import lombok.Setter

@Getter
@Setter
class ReleaseRes {
    var releases = mutableListOf<Release>()
    var description: String? = null
}