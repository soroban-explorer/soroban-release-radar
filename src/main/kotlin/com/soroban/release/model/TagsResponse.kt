package com.soroban.release.model

data class TagsResponse(val results: List<Tag>, val count: Int)