package com.soroban.release.util

object Constants {
    const val REPO_RUST = "rust"
    const val REPO_SOROBAN = "stellar/soroban-cli"
}