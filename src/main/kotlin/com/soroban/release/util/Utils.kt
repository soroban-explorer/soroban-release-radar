package com.soroban.release.util

import org.springframework.core.env.Environment
import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*

object Utils {
    fun getCurrent(): Date? {
        return Timestamp.valueOf(LocalDateTime.now())
    }
    fun getConfigVal(key: String, env: Environment): String? {
        val sysVal = System.getProperty(key)
        return if (sysVal != null) {
            sysVal
        } else {
            env.getProperty(key)
        }
    }
}