package com.soroban.release

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan(basePackages = ["com.soroban"])
class SorobanReleaseRadarApplication

fun main(args: Array<String>) {
	runApplication<SorobanReleaseRadarApplication>(*args)
}